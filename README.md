# README #
MagnetoServer jest serwerem odbierającym dane magnetometryczne (i nie tylko) z innych urządzeń. Wykorzystuje protokół
TCP/IP, działa na porcie 9269. Wymaga do pracy Java JRE 1.7 lub nowszej wersji.