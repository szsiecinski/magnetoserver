package org.bitbucket.szsiecinski.magnetoserver;

import java.util.concurrent.*;

public class Program {

	public static void main(String[] args) {
		//INFO: domyślny port: 9269
		Server s = Server.incjalizujSerwer();
		Executors.newSingleThreadExecutor().execute(s);
	}

}
