package org.bitbucket.szsiecinski.magnetoserver;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.concurrent.*;
import java.util.*;

public class Server implements Runnable{

	private static Server s;
	private final static int DOMYSLNY_PORT = 9269;
	private ServerSocket gniazdoServera;
	
	class AsystentServera implements Runnable {

		BufferedReader strumien_wejscia;
		Socket gniazdo;

		AsystentServera(Socket gniazdo) {
			this.gniazdo = gniazdo;
		}

		@Override
		public void run() {

			try {
				strumien_wejscia = new BufferedReader(new InputStreamReader(
						gniazdo.getInputStream()));

			} catch (IOException e) {
				e.printStackTrace();
			}
			
			String odebranyText;
			
			try {
				while((odebranyText = strumien_wejscia.readLine()) != null) {
					
					Date dataOdbioru = new Date(System.currentTimeMillis());
					SimpleDateFormat sdf = new SimpleDateFormat(" yyyy-MM-dd 'o' HH:mm:ss");
					
					String dane_textowe = String.format("Wysłane z %s %s: %s",
							gniazdo.getInetAddress().toString(), sdf.format(dataOdbioru), odebranyText);
					
					System.out.println(dane_textowe);

				}
			} catch (IOException e) {
				
			}
		}

	}
	
	@Override
	public void run() {
		Executor polaczenia = Executors.newCachedThreadPool();
		
		while(true) {
			try {
				Socket nasluchiwacz = gniazdoServera.accept();
				polaczenia.execute(new AsystentServera(nasluchiwacz));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}
		}
	}
	
	protected Server() throws IOException {
		this(DOMYSLNY_PORT);
	}

	protected Server(int port) throws IOException {
		gniazdoServera = new ServerSocket(port);
	}
	
	public static Server incjalizujSerwer() {
		if(s==null) {
			try {
				s = new Server();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return s;
	}
	
	public static Server incjalizujSerwer(int port) {
		if(s==null) {
			try {
				s = new Server(port);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return s;
	}
	
	public void ZakonczDzialanie() throws IOException {
		gniazdoServera.close();
	}
}
